SHELL = /bin/zsh
OS := $(shell bin/is-supported bin/is-macos macos linux)
DOTFILES_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
HOMEBREW_PREFIX := $(shell bin/is-supported bin/is-arm64 /opt/homebrew /usr/local)
PATH := $(HOMEBREW_PREFIX)/bin:$(DOTFILES_DIR)/bin:$(PATH)
# export XDG_CONFIG_HOME = $(HOME)/.config
# export STOW_DIR = $(DOTFILES_DIR)
# export ACCEPT_EULA=Y

MASFILE := ${DOTFILES_DIR}/install/AppFile
BREWFILE := ${DOTFILES_DIR}/install/BrewFile
CASKFILE := ${DOTFILES_DIR}/install/CaskFile
CODEFILE := ${DOTFILES_DIR}/install/CodeFile

dotfiles:
	@echo $#
	@echo "Dotfiles:" ${DOTFILES_DIR}
	@echo "OS:" ${OS}
	@echo "Homebrew Prefix:" ${HOMEBREW_PREFIX}

all: $(OS)

macos: sudo core-macos packages

sudo:
ifndef GITHUB_ACTION
	sudo -v
	while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &
endif

core-macos: brew git npm
packages: brew-packages cask-apps node-packages

brew:
	is-executable brew || curl -fsSL "https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh" | NONINTERACTIVE=1 bash

git: brew
	brew install git git-extras

npm: brew-packages
	fnm install --lts

brew-packages: brew
	brew bundle --file=$(DOTFILES_DIR)/install/Brewfile || true

cask-apps: brew
	brew bundle --file=$(DOTFILES_DIR)/install/Caskfile || true
	defaults write org.hammerspoon.Hammerspoon MJConfigFile "~/.config/hammerspoon/init.lua"
	for EXT in $$(cat install/Codefile); do code --install-extension $$EXT; done
	xattr -d -r com.apple.quarantine ~/Library/QuickLook

node-packages: npm
	eval $$(fnm env); npm install -g $(shell cat install/npmfile)

.ONESHELL:
mas-apps: brew
	while IFS=$$'\t ' read -r appId second third fourth; do \
  		if [[ $${appId:0:1} != '#' ]]; then \
  		  mas install $${appId}; \
	  	fi; \
	done < $(DOTFILES_DIR)/install/AppFile

.ONESHELL:
git-config:
	@if [[ ${OS} == 'macos' ]]; then \
    	git config --global credential.helper osxkeychain; \
    else \
    	git config --global credential.helper cache; \
    fi; \
    if command -v code >/dev/null 2>&1; then \
    	git config --global merge.tool vscode; \
    	git config --global mergetool.vscode.cmd "code --wait $MERGED"; \
    fi; \

.ONESHELL:
git-config2:
	if command -v code >/dev/null 2>&1 ]]; then \
		git config --global merge.tool vscode; \
		git config --global mergetool.vscode.cmd "code --wait $MERGED"; \
	fi; \
