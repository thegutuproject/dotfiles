# Some basic env setup used throughout config
# this seems to throw some weird error 
# failed to load module `zsh/pcre
# -pcre-match not available for regex
# [[ ${OSTYPE} =~ ^darwin[0-9]{2}\.[0-9]{1} ]]; IS_MAC2=$?
[[ $(uname) == "Darwin" ]]; IS_MAC=$?

# Enable hooks
typeset -gUa preexec_functions
typeset -gUa precmd_functions
typeset -gUa chpwd_functions

fpath=($DOTFILES/functions $fpath)
autoload -U $DOTFILES/functions/*(:t)

# For historical purposes.
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
REPORTTIME=60               # print elapsed time when more than 60 seconds

# https://github.com/cehoffman/dotfiles/blob/master/zsh/config
setopt AUTOCD               # allows to navigate to folder by name only without cd
setopt NO_LIST_BEEP
setopt LOCAL_OPTIONS        # allow functions to have local options
setopt LOCAL_TRAPS          # allow functions to have local traps
setopt CORRECT              # turns on spelling correction for commands
setopt NO_CORRECT_ALL       # Prevents asking 'correct git to .git' when at root
setopt COMPLETE_IN_WORD
# setopt NO_ALWAYS_TO_END     # Don't got to end of word on completion from middle
setopt NO_LIST_AMBIGUOUS    # Show completion list immediately when ambiguous
# setopt NO_REC_EXACT         # Always ask if multiple matches exist with word

# setopt AUTO_MENU
setopt LIST_ROWS_FIRST      # Order completion by row instead of column
setopt RC_EXPAND_PARAM      # Use arrays in in params to get multiple versions
setopt CLOBBER              # allow > to clobber and >> to append/create
setopt POSIX_BUILTINS       # allow `command` to use the builtin versions

setopt INTERACTIVE_COMMENTS # Allow comments when in an interactive shell
setopt NO_PRINT_EXIT_VALUE  # My zsh prompt tells me what the exit value was

setopt SHARE_HISTORY        # All my shells are like one hive mind, cool
setopt HIST_VERIFY          # Verity a history command before executing (sanity)
setopt HIST_IGNORE_ALL_DUPS # Don't record dupes in history
setopt HIST_REDUCE_BLANKS   # Remove blanks between arguments before saving
setopt HIST_IGNORE_SPACE    # Insert a space at start to prevent history of cmd
setopt HIST_NO_STORE        # Don't save the history command (fc -l) in history
setopt HIST_LEX_WORDS       # Parse history from file like it was on commandline

setopt CHECK_JOBS           # A little nag that jobs exist before quiting
setopt NO_HUP               # Used in combination with CHECK_JOBS
setopt AUTO_CONTINUE        # Disowned jobs begin to run again
setopt LONG_LIST_JOBS       # Obvious, longer list of jobs
setopt NO_NOTIFY            # For an easier to read terminal, wait till prompt
setopt NO_AUTO_RESUME       # Bad inference, I don't mean to resume jobs
setopt NO_POSIX_JOBS        # Sub shells have their own job lists
setopt NO_BG_NICE           # don't nice background tasks

setopt AUTO_CD              # Don't require using cd to cd
setopt AUTO_PUSHD           # every cd pushs onto the stack
setopt PUSHD_SILENT         # Silent cds since we pushd
setopt PUSHD_IGNORE_DUPS    # Ignore duplicate pushs
setopt PUSHD_TO_HOME        # Empty pushd is like cd

setopt NULL_GLOB            # No match return nil, used in $path
setopt EXTENDED_GLOB        # use regex style globing
setopt REMATCH_PCRE         # use perl regex matching for the =~ operator

setopt C_BASES              # Output non base 10 like C

setopt AUTO_PARAM_KEYS      # Neater prompt, deletes spaces after complete
setopt AUTO_PARAM_SLASH     # Params that are directories get a /
setopt NOAUTO_REMOVE_SLASH  # Keep a slash if I put it there

setopt MAGIC_EQUAL_SUBST    # option=arg can have arg path expanded
setopt PROMPT_SUBST         # Allow variable substitution in the prompt

setopt IGNORE_EOF           # Allow ^D to be bound, 10 consecuritve EOF will quit
setopt NOFLOW_CONTROL       # Allow biding of ^S/^Q

# don't expand aliases _before_ completion has finished
#   like: git comm-[tab]
setopt complete_aliases

bindkey '^[^[[D' backward-word
bindkey '^[^[[C' forward-word
bindkey '^[[5D' beginning-of-line
bindkey '^[[5C' end-of-line
bindkey '^[[3~' delete-char
bindkey '^?' backward-delete-char