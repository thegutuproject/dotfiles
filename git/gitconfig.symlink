[user]
	name = Alexandru Gutu
	email = alex.s.gutu@gmail.com

[github]
	user = thegutuproject

[apply]
	whitespace = nowarn

[core]
	excludesfile = ~/.gitignore
	editor = code --wait
  	filemode = false
  	trustctime = false
  	autocrlf = input
  	untrackedCache = true
  	pager = diff-so-fancy | less --tabs=4 -RFX
  	ignorecase = false
	compression = -1
	whitespace = trailing-space,space-before-tab
	precomposeunicode = true

[credential]
	helper = osxkeychain

[color]
	diff = auto
	status = auto
	branch = auto
	ui = true

[pull]
	rebase = true

[init]
	defaultBranch = main

[push]
	default = simple
  followTags = true

[fetch]
    prune = true

[grep]
	extendRegexp = true
	lineNumber = true

[help]
	autocorrect = 1

[alias]
	amend = commit --amend --reuse-message=HEAD
  	br = branch
  	ci = commit
	co = checkout
	changed = show --pretty=\"format:\" --name-only
  	contrib = shortlog --summary --numbered
	count = shortlog -sn
  	cr = clone --recursive
  	df = diff --word-diff
	fpr = fetch-pr
	g = grep --break --heading --line-number
	gi = grep --break --heading --line-number -i
	lt = log --tags --decorate --simplify-by-decoration --oneline
  	l = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
  	ld = "!sh -c \"git log --since '${1:-1} days ago' --oneline --author $(git config user.email)\" -"
  	lg = log -p
  	ll = log --pretty=oneline --graph --abbrev-commit
  	lm = log --pretty=format:'* %s (%h)'
	fm = fetch-merge
	pr = "!f() { git fetch -fu ${2:-origin} refs/pull/$1/head:pr/$1 && git checkout pr/$1; }; f"
	please = push --force-with-lease
	commend = commit --amend --no-edit
  	patch = !git --no-pager diff --no-color
  	p = push
  	pf = push --force
  	show-ignored = "! git clean -ndX | perl -pe 's/Would remove/Ignored:/'"
  	st = status
  	stl = ls-files -m -o --exclude-standard
  	sts = status -sb
  	unstage = reset --hard HEAD

[diff]
	tool = diffmerge
  	renames = copies
  	indentHeuristic = true
[difftool]
  	prompt = false
[difftool "diffmerge"]
  	cmd = diffmerge $LOCAL $REMOTE
[difftool "vscode"]
  	cmd = "code --wait "

[merge]
	tool = vscode
[mergetool "diffmerge"]
	cmd = diffmerge --merge --result=$MERGED $LOCAL $BASE $REMOTE
	trustExitCode = true
	keepBackup = false
[mergetool "vscode"]
# Comment: Original way before three-way merge shown commented out
	cmd = code --wait MERGED
# Comment: For "Three-way merge"
#   cmd = code --wait --merge $REMOTE $LOCAL $BASE $MERGED
[color]
	ui = auto
[color "branch"]
	current = yellow reverse
	local = yellow
	remote = green
[color "diff"]
	meta = yellow bold
	frag = magenta bold
	old = red bold
	new = green bold
[color "status"]
	added = yellow
	changed = green
	untracked = cyan
[color "diff-highlight"]
	oldNormal = red bold
	oldHighlight = red bold 52
	newNormal = green bold
	newHighlight = green bold 22

[submodule]
	fetchJobs = 4

[commit]
	template = ~/.dotfiles/git/template.txt

[log]
	showSignature = false

[includeIf "gitdir:~/Documents/Code/"] # for work
	path = ~/.dotfiles/git/icf.gitconfig
