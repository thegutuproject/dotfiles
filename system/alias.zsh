# Shortcuts

alias reload='. ~/.zshrc'
# Good 'ol Clear Screen command
alias cls='clear'
alias lsa='lsd -al'

alias antidote_bundle="antidote bundle < $DOTFILES/antidote/zsh_plugins.txt > $DOTFILES/antidote/zsh_plugins.zsh"
alias antidote_rebundle="rm $DOTFILES/antidote/zsh_plugins.zsh"

# Pipe my public key to my clipboard.
alias pubkey="more ~/.ssh/id_rsa.pub | pbcopy | echo '=> Public key copied to pasteboard.'"

alias reload="source ~/.zshrc"
alias _="sudo"
alias g="git"
alias rr="rm -rf"

# Default options

alias rsync="rsync -vh"
alias json="json -c"
alias psgrep="psgrep -i"

# Global aliases

if $(is-supported "alias -g"); then
  alias -g G="| grep -i"
  alias -g H="| head"
  alias -g T="| tail"
  alias -g L="| less"
fi

# List declared aliases, functions, paths

alias aliases="alias | sed 's/=.*//'"
alias functions="declare -f | grep '^[a-z].* ()' | sed 's/{$//'"
alias paths='echo -e ${PATH//:/\\n}'

# Directory listing/traversal

LS_COLORS=$(is-supported "ls --color" --color -G)
LS_TIMESTYLEISO=$(is-supported "ls --time-style=long-iso" --time-style=long-iso)
LS_GROUPDIRSFIRST=$(is-supported "ls --group-directories-first" --group-directories-first)

alias l="ls -lahA $LS_COLORS $LS_TIMESTYLEISO $LS_GROUPDIRSFIRST"
alias ll="ls -lA $LS_COLORS"
alias lt="ls -lhAtr $LS_COLORS $LS_TIMESTYLEISO $LS_GROUPDIRSFIRST"
alias ld="ls -ld $LS_COLORS */"
alias lp="stat -c '%a %n' *"

unset LS_COLORS LS_TIMESTYLEISO LS_GROUPDIRSFIRST

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias -- -="cd -"                  # Go to previous dir with -
alias cd.='cd $(readlink -f .)'    # Go to real dir (i.e. if current dir is linked)

# npm

alias ni="npm install"
alias nu="npm uninstall"
alias nri="rm -r node_modules && npm install"
alias ncd="npm-check -su"

# Network

alias ip="curl -s ipinfo.io | jq -r '.ip'"
alias ipl="ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1'"

# Miscellaneous

alias hosts="sudo $EDITOR /etc/hosts"
alias quit="exit"
alias week="date +%V"
alias speedtest="wget -O /dev/null http://speed.transip.nl/100mb.bin"
alias grip="grip --browser --user=webpro --pass=$GITHUB_TOKEN"





[[ -z "$TMUX" ]] && alias tmux="tmux attach-session"
function mkcd() { mkdir -p "$1" && cd "$1" }

dclean() {
  docker ps -q -f status=exited | xargs -r docker rm
  docker images -q -f dangling=true | xargs -r docker rmi
}

function debug() {
  set -x; "$@"; set +x
}

function 256c() {
  echo -n "\e[38;5;${1}m${${(z)*}[2,-1]}"
}
