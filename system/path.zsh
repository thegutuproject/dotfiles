# Figure out how to add man paths better
export MANPATH="/usr/local/man:/usr/local/mysql/man:/usr/local/git/man:$MANPATH"
# Start with system path and add our own to $PATH.
# Retrieve it from getconf, otherwise it's just current $PATH
is-executable getconf && PATH=$($(command -v getconf) PATH):${DOTFILES}/bin

if is-executable yarn > /dev/null 2>&1; then
  prepend-path $(yarn global bin)
fi

# Prepend new items to path (if directory exists)
prepend-path "/bin"
prepend-path "/usr/bin"
prepend-path "/usr/local/bin"
prepend-path "$HOMEBREW_PREFIX/bin"
prepend-path "$HOMEBREW_PREFIX/opt/coreutils/libexec/gnubin"
prepend-path "$HOMEBREW_PREFIX/opt/gnu-sed/libexec/gnubin"
prepend-path "$HOMEBREW_PREFIX/opt/grep/libexec/gnubin"
prepend-path "$HOMEBREW_PREFIX/opt/python/libexec/bin"
prepend-path "$HOMEBREW_PREFIX/opt/ruby/bin"
prepend-path "$DOTFILES_DIR/bin"
prepend-path "$HOME/bin"
prepend-path "$HOME/.cargo/bin"
prepend-path "/sbin"
prepend-path "/usr/sbin"
prepend-path "/usr/local/sbin"

# XCode stuff
prepend-path "/Library/Apple/usr/bin"

export PATH
