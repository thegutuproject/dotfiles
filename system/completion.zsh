##!/bin/zsh
## forces zsh to realize new commands
#zstyle ':completion:*' completer _oldlist _expand _complete _match _ignored _approximate
#
## matches case insensitive for lowercase
#zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
#
## provide .. as a completion
#zstyle ':completion:*' special-dirs ..
#
## pasting with tabs doesn't perform completion
#zstyle ':completion:*' insert-tab pending
#
## use caching
#zstyle ':completion:*' use-cache true
## zstyle ':completion::complete:*' cachepath .zcache
#
## Use a menu for completion
#zstyle ':completion:*' menu select
## menu if nb items > 2
## zstyle ':completion:*' menu select=2
#
## rehash if command not found (possibly recently installed)
#zstyle ':completion:*' rehash true
#
## Describe options in full
#zstyle ':completion:*:options' auto-description '%d'
#zstyle ':completion:*:options' description 'yes'
#
## Make the process selection a little nicer
#zstyle ':completion:*:*:kill:*:processes' list-colors  '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
#zstyle ':completion:*:*:*:*:processes' command "ps -au $USER -o pid,user,comm -w -w"
#
## Better history
## Credits to https://coderwall.com/p/jpj_6q/zsh-better-history-searching-with-arrow-keys
#autoload -U up-line-or-beginning-search
#autoload -U down-line-or-beginning-search
#zle -N up-line-or-beginning-search
#zle -N down-line-or-beginning-search
#bindkey "^[[A" up-line-or-beginning-search # Up
#bindkey "^[[B" down-line-or-beginning-search # Down
#
## pipx autocomplete
## eval "$(register-python-argcomplete pipx)"
#
#
## https://github.com/cehoffman/dotfiles/blob/master/zsh/completion
## Don't ever select the parent directory, e.g. cd ../<TAB> won't select your current dir
#zstyle ':completion:*:cd:*' ignore-parents parent pwd
#
## With commands like rm, it's annoying if you keep getting offered the same
## file multiple times. This fixes it. Also good for cp, et cetera..
#zstyle ':completion:*:(rm|cp):*' ignore-line yes
#
#zstyle -e ':completion:*' completer _oldlist _expand _complete _match _ignored _approximate _list _history _files
#
## Some completion settings
## zstyle ':completion:*:match:*' match-original only
#zstyle ':completion:*' insert-unambiguous true
#
## Prevent zsh from treating some//path like some/*/path for completion
#zstyle ':completion:*' squeeze-slashes true
#
## verbose completion
#zstyle ':completion:*' verbose true
#zstyle ':completion:*:descriptions' format '%F{green}Completions for %B%d%b%f'
#zstyle ':completion:*:messages' format '%d'
#zstyle ':completion:*:warnings' format 'No matches for: %d'
#zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
#zstyle ':completion:*:approximate:*' original true
#zstyle ':completion:*:approximate:*' prompt 'Approximate to: %e'
#
## Don't autoexpand until nothing else has changed on line
#zstyle ':completion:*' keep-prefix true
#zstyle ':completion:*' expand prefix suffix
## zstyle ':completion:*' suffix true
#
## Separate matches into groups because empty string tells system to use name
## of completion function used as name of group
#zstyle ':completion:*' group-name ''
#
## Ignore completions functions for commands you don't have
#zstyle ':completion::(^approximate*):*:functions' ignored-patterns '_*'
#
## Make approximate allow more errors as the length increases
#zstyle -e ':completion:*:approximate:*' max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3))numeric)'
#
## Suggested completion style from _git completion
#zstyle ':completion::*:git-{name-rev,add,rm}:*' ignore-line true
#
## Add a recent director listing completion using the cdr command
#autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
#add-zsh-hook chpwd chpwd_recent_dirs
#zstyle ':completion:*:*:cdr:*:*' menu selection
#zstyle ':chpwd:*' recent-dirs-default true
#zstyle ':completion:*' recent-dirs-insert both
#zstyle ':chpwd:*' recent-dirs-prune parent
#
#zmodload -i zsh/complist
#
#function {
#  emulate -L zsh
#  local hash
#  for hash in md5 sha1 sha256 sha512; compdef _files $hash
#}
#
## [[ $IS_MAC = 0 ]] && compdef _man pman
## compdef _ls l
## _git # Need to call _git to load _git-diff and other functions - this is slow
## compdef g=git
## compdef _git-diff gd
## compdef _git-add ga
## compdef _git-add gap
## compdef _git-status gs
## compdef _git-commit gc
## compdef _git-checkout gco
## compdef _git-push gp
## compdef _git-log gl
## (( $+commands[gpg2] )) && compdef gpg2=gpg
#
## vim: filetype=zsh

# Dotfiles
_dotfiles_completions() {
  local cur="${COMP_WORDS[COMP_CWORD]}"
  COMPREPLY=( $(compgen -W 'clean dock edit help macos test update' -- $cur ) );
}

complete -o default -F _dotfiles_completions dot

# npm (https://docs.npmjs.com/cli/completion)
if is-executable npm; then

fi

# fnm
if is-executable fnm; then
  # doesnt work...
  FPATH=${DOTFILES}/completion/fnm.completion:$FPATH
fi

if is-executable brew; then
  FPATH=$(brew --prefix)/share/zsh-completions:$FPATH
  autoload -Uz compinit && compinit
fi
