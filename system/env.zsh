# I like to use VSCode; for now.
export EDITOR=code

# Eastern timezone, what's up?
export TZ=America/New_York

# Local Node Modules bin.
export LOCAL_NODE_MODULES_BIN="./node_modules/.bin"

export LSCOLORS="exfxcxdxbxegedabagacad"
export CLICOLOR=true
