This contains my person dot files. 

---

Use `ln -s ~/.files/zshrc ~/.zshrc` to store files in one spot

Reference: `ln -s $SRC $DEST`

---

Download the zip file, extract to `~/.dotfiles`

To start the restore process use `initialization.sh`  
Finish using `installations.sh`

If they don't execute, you may need to use `chmod +x {SCRIPT}`
