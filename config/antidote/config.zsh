source "$HOMEBREW_PREFIX/opt/antidote/share/antidote/antidote.zsh"

ANTIDOTE_CONFIG="$DOTFILES"/config/antidote

if [ -f "$ANTIDOTE_CONFIG/zsh_plugins.zsh" ] && [ -s "$ANTIDOTE_CONFIG/zsh_plugins.zsh" ]; then
  source "$ANTIDOTE_CONFIG/zsh_plugins.zsh"
else
  # zsh_plugins.zsh does not yet exist. Create and load it.
  echo "Rebundling antidote"
  antidote bundle < "$ANTIDOTE_CONFIG/zsh_plugins.txt" > "$ANTIDOTE_CONFIG/zsh_plugins.zsh"
  source "$ANTIDOTE_CONFIG/zsh_plugins.zsh"
fi
