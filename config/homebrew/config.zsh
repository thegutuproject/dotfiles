if $DOTFILES/bin/is-executable brew; then
  # Cask needs to keep all applications together.
  export HOMEBREW_CASK_OPTS="--appdir=/Applications"

  # Disable analytics
  export HOMEBREW_NO_ANALYTICS=1
fi
